#!/bin/sh
a=$(docker ps -a | grep vpn | awk '{print $1}')
if [ -n "$a" ];then
    docker rm -f vpn
fi
docker run -d --name vpn --privileged --net=host -p 1723:1723 -v ~/docker/vpn-pptp/chap-secrets:/etc/ppp/chap-secrets vpn
